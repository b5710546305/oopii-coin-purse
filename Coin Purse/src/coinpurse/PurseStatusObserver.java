package coinpurse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JProgressBar;

/**
 * Display status of the purse
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.01
 *
 */
public class PurseStatusObserver extends PurseObserver {

	private JPanel contentPane;
	public static JLabel capacity;
	public static JLabel status;
	public static JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PurseStatusObserver frame = new PurseStatusObserver();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PurseStatusObserver() {
		setTitle("Purse Status");
		setResizable(false);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(700,500,300,100);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblStatus = new JLabel("Capacity : ");
		lblStatus.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		lblStatus.setBounds(10, 11, 81, 32);
		contentPane.add(lblStatus);
		
		capacity = new JLabel("0/5");
		capacity.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		capacity.setBounds(101, 11, 92, 32);
		contentPane.add(capacity);
		
		status = new JLabel("EMPTY");
		status.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		status.setBounds(203, 11, 89, 32);
		contentPane.add(status);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(0, 54, 292, 14);
		contentPane.add(progressBar);
	}
}
