package coinpurse;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

/**
 * PurseObserver lets users see the status pf the purse
 * in a console or a window
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.01
 */
public class PurseObserver extends JFrame implements Observer{

	private JPanel contentPane;
	private JTextField deposit_txt;
	private JTextField with_txt;
	public JLabel set;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PurseObserver frame = new PurseObserver();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Constructor
	 */
	public PurseObserver(){
		super("Purse");
		setResizable(false);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300,300,700,100);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		deposit_txt = new JTextField();
		deposit_txt.setBounds(180, 25, 105, 20);
		//contentPane.add(deposit_txt);
		deposit_txt.setColumns(10);
		
		with_txt = new JTextField();
		with_txt.setText("");
		with_txt.setBounds(530, 25, 105, 20);
		//contentPane.add(with_txt);
		with_txt.setColumns(10);
		
		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.setBounds(55, 24, 100, 23);
		//contentPane.add(btnDeposit);
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.setBounds(405, 24, 100, 23);
		
		JLabel lblPurseContains = new JLabel("Purse Contains :");
		lblPurseContains.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPurseContains.setBounds(47, 19, 130, 29);
		contentPane.add(lblPurseContains);
		
		set = new JLabel("{}");
		set.setFont(new Font("Tahoma", Font.BOLD, 14));
		set.setBounds(187, 19, 452, 29);
		contentPane.add(set);
		
		btnDeposit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		btnWithdraw.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
	
	/**
	 * update recieves notification from the purse
	 * @param subject
	 * @param info
	 */
	public void update(Observable subject, Object info) {
		if (subject instanceof Purse){
			Purse purse = (Purse)subject;

			//Observer Function
			PurseBalanceObserver.balance.setText(String.format("%.2f", purse.getBalance()));
			PurseStatusObserver.capacity.setText(purse.count()+"/"+purse.getCapacity());
			if(purse.count() == 0){
				PurseStatusObserver.status.setText("EMPTY");
			} else if (purse.isFull()){
				PurseStatusObserver.status.setText("FULL");
			} else {
				PurseStatusObserver.status.setText("NORMAL");
			}
			PurseStatusObserver.progressBar.setValue((int)(((double)purse.count()/(double)purse.getCapacity())*100));
			set.setText(purse.toString());
			
		}
		if (info != null) System.out.println(info);
		
	}
	
	/**
	 * Run GUI
	 */
	public void run(){
		main(new String[] {"0"});
		PurseBalanceObserver.main(new String[] {"0"});
		PurseStatusObserver.main(new String[] {"0"});
	}
}
