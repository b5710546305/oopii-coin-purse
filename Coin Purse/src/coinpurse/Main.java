package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.01
 */
public class Main {

    /**
     * @param args not used 
     */
    public static void main( String[] args ) {
        // 1. create a Purse
    	Purse purse = new Purse(5);
    	WithdrawStrategy ws = new GreedyWithdraw();
    	purse.setWithdrawStrategy(ws);
    	
    	//Purse Observer creation
    	PurseObserver obs = new PurseObserver();
    	purse.addObserver(obs);
    	obs.run();
    	
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog cd = new ConsoleDialog(purse);
    	
        // 3. run() the ConsoleDialog
    	cd.run();

    }
}
