package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 *  The Recursive way of withdrawal
 *  Gains more advantage than Greedy Algorithm for 
 *  withdrawing 6 in purse {2,2,2,5} for example
 *  @author  Parinvut Rochanavedya 
 *  @version 2015.02.17
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	
	/**
	 * Withdraw valuables from the list with recursive algorithm
	 * @param amount amount to withdraw
	 * @param valuables list of valuables
	 * @return withdrawal list of valuables
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> valuables) {
		
		if (amount <= 0) { return null;}
		
		List<Valuable> tempList = new ArrayList<Valuable>();
		
		tempList.addAll(valuables);
		
		List<Valuable> withList = withdrawFrom(amount,tempList);
		
		valuables.removeAll(withList);
		
		return withList;
		
	}
	
	/**
	 * The recursive method
	 * @param amount amount to withdraw
	 * @param valuables list of valuables
	 * @param index of valuables
	 * @return the list of withdrawal
	 */
	public List<Valuable> withdrawFrom(double amount, List<Valuable> context){
		List<Valuable> withList = new ArrayList<Valuable>();
		
		int lastIndex = 0;
		if(context.size() > 1){
			lastIndex = context.size()-1;
		}
		lastIndex = 0;
		withList.add(context.get(lastIndex));
		double withdrawValue = context.get(lastIndex).getValue();
		amount -= withdrawValue;
	
		if(amount < 0){return null;}
		
		context.remove(lastIndex);
		
		if(amount == 0){return withList;}
		
		List<Valuable> extraWithList = withdrawFrom(amount,context); //Recursion occurs here
		
		if(extraWithList == null){
			withList = new ArrayList<Valuable>();
			amount += withdrawValue;
			extraWithList = withdrawFrom(amount,context); //Recursion occurs here (fix amount in new context)
		}
		withList.addAll(extraWithList);
		
		return withList;
		
	}

}