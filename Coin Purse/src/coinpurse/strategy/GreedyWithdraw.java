package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 *  The Greedy way of withdrawal
 *  picking the biggest value first then the smallest
 *  make sure the amount fits with the existing values
 *  @author  Parinvut Rochanavedya 
 *  @version 2015.02.17
 */
public class GreedyWithdraw implements WithdrawStrategy{

	/**Comparator Object*/
    private final ValueComparator comparator = new ValueComparator();
	
	/**
	 * Withdraw valuables from the list with greedy algorithm
	 * @param amount amount to withdraw
	 * @param valuables list of valuables
	 * @return withdrawal list of valuables
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> valuables) {
		if ( amount < 0 ) return null;
        
		
        //The temporary list
        List<Valuable> temp = new ArrayList<Valuable>(); 
        for(int i = 0; i < valuables.size(); i++){
        	temp.add(valuables.get(i));
        }
        
        //sort valuableList from highest to lowest
        Collections.sort(valuables, comparator);
        Collections.reverse(valuables);
        
        //Withdraw List
        List<Valuable> withList = new ArrayList<Valuable>(); 
        
        //Get the Most Value Coin
        for(int valuableToAdd = 0; valuableToAdd < valuables.size(); valuableToAdd++){
        	
        	//check amount
        	if(amount >= valuables.get(valuableToAdd).getValue()){
        		//add to withdraw list
        		withList.add(valuables.get(valuableToAdd));
        		//remove from parameter amount value
        		amount -= valuables.get(valuableToAdd).getValue();
        		//remove from temporary list
            	temp.remove(valuables.get(valuableToAdd));
        	}
        	
        }
        
        

		// Did we get the full amount?
		if ( amount > 0 )
		{	// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			return null;
		}

		// Success.
		// Since this method returns an array of Coin,
		// create an array of the correct size and copy
		// the Coins to withdraw into the array.
		for(int i = 0; i < withList.size(); i++){
			valuables.remove(withList.get(i));
		}
        return withList;
		
	}

}
