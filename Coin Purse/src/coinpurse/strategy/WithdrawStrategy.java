package coinpurse.strategy;

import java.util.List;
import coinpurse.Valuable;

/**
 * 	An interface of withdrawal strategies
 *  @author  Parinvut Rochanavedya 
 *  @version 2015.02.17
 */
public interface WithdrawStrategy {
	/**
	 * Withdraw valuables from the list
	 * @param amount amount to withdraw
	 * @param valuables list of valuables
	 * @return withdrawal list of valuables
	 */
	List<Valuable> withdraw(double amount, List<Valuable> valuables);
}
