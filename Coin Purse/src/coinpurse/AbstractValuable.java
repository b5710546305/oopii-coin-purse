package coinpurse;
/**
 * 
 * An Abstract Valuable super class for sub classed with a monetary value.
 * You can't change the value.
 * @author  Parinvut Rochanavedya 
 * @version 2015.02.10
 */
public abstract class AbstractValuable implements Valuable{

	/** Value of the Valuable */
	protected double value;
	
	/**
     * Check if the values of this Valuable is equals to the value of 
     * another object or not
     * @param other is the value for any other object to compare
     * @return true if the value is the same
     */
    public boolean equals(Object other){
    	if(!(this.getClass() == other.getClass())) return false;
    	return this.getValue() == (((AbstractValuable)other).getValue());
    }
    
    /**
     * Return the value of the Valuable in decimals
     * @return the value
     */
    public double getValue(){
    	return value;
    }
    
    /**
     * Return the difference value of this Valuable and another Valuable 
     * in the parameter
     * @param other is the value for any other Valuable to compare
     * @return difference value of this Valuable and another Valuable
     */
    public int compareTo(Valuable other){
    	double current = this.getValue();
    	double another = other.getValue();
    	if(this.getValue() < 0){
    		current = 0;
    	}
    	if(this.getValue() > Integer.MAX_VALUE){
    		current = Integer.MAX_VALUE;
    	}
    	if(this.getValue() < 1 && this.getValue() > 0){
			current = 0;
		}
    	if(other.getValue() < 0){
    		another = 0;
    	}
    	if(other.getValue() > Integer.MAX_VALUE){
    		another = Integer.MAX_VALUE;
    	}
    	if(other.getValue() < 1 && other.getValue() > 0){
    		another = 0;
		}
    	return (int)(current - another);
    }
	
	
}
