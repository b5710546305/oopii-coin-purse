package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author  Parinvut Rochanavedya 
 * @version 2015.02.10
 */
public class Coin extends AbstractValuable{

    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }

    
    /**
     * Print out the coin and it's value
     */
    public String toString(){
    	return value + " coin";
    }
    
    
}
