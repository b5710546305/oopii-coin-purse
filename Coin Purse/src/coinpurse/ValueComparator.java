package coinpurse;
import java.util.Comparator;

/**
 * A comparator object class for valuable objects
 * @author  Parinvut Rochanavedya 
 * @version 2015.01.27
 */
public class ValueComparator implements Comparator<Valuable> {
	/**
	 * Compare\
	 * @param Valuable objects a and b
	 * @return 1 if a.value > b.value, 0 if a.value = b.value, -1 if a.value < b.value
	 */
	public int compare(Valuable a, Valuable b) {
		// compare them by value. This is easy.
		if (a.getValue() > b.getValue()){
			return 1;	
		} else if (a.getValue() == b.getValue()){
			return 0;
		} else {
			return -1;
		}
	}
}
