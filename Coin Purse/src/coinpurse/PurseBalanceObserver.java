package coinpurse;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;

/**
 * Display balance of the purse
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.01
 */
public class PurseBalanceObserver extends PurseObserver {

	private JPanel contentPane;
	public static JLabel balance;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PurseBalanceObserver frame = new PurseBalanceObserver();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PurseBalanceObserver() {
		setTitle("Purse Balance");
		setResizable(false);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300,500,300,100);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBaht = new JLabel("Baht");
		lblBaht.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		lblBaht.setBounds(225, 11, 47, 32);
		contentPane.add(lblBaht);
		
		JLabel lblBalance = new JLabel("Balance : ");
		lblBalance.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		lblBalance.setBounds(20, 11, 81, 32);
		contentPane.add(lblBalance);
		
		balance = new JLabel("0");
		balance.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		balance.setBounds(111, 11, 86, 32);
		contentPane.add(balance);
	}
}
