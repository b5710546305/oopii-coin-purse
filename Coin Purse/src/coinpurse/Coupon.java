package coinpurse;
import java.util.HashMap;
import java.util.Map;

/**
 * A coupon with a color value.
 * You can't change the value of a coupon.
 * @author  Parinvut Rochanavedya 
 * @version 2015.02.10
 */
public class Coupon extends AbstractValuable{

	/** Value of the coupon */
    private static Map<String,Double> map = new HashMap<String,Double>();
    
    static {
        map.put("red", 100.0);
        map.put("blue", 50.0);
        map.put("green", 20.0);
    }
    
    /** 
     * Constructor for a new coupon. 
     * @param value is the value for the coupon
     */
    public Coupon( String value ) {
    	this.value = map.get(value);
    }

    
    /**
     * Print out the coin and it's value
     */
    public String toString(){
    	return value + " coupon";
    }

}
