package coinpurse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import coinpurse.strategy.WithdrawStrategy;

/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author  Parinvut Rochanavedya 
 *  @version 2015.03.01
 */
public class Purse extends Observable{
    /** Collection of valuables in the purse. */
    private List<Valuable> valuableList;
    
    /**The Withdrawal Strategy object not specified the type*/
    private WithdrawStrategy strategy;
    
    /**Comparator Object*/
    private final ValueComparator comparator = new ValueComparator();

    
    /** Capacity is maximum NUMBER of valuables the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of valuables you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	valuableList = new ArrayList<Valuable>();
    	
    }

    /**
     * Count and return the number of valuables in the purse.
     * This is the number of valuables, not their value.
     * @return the number of valuables in the purse
     */
    public int count() { return valuableList.size(); }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance = 0;
    	for(int i = 0; i < count(); i++){
    		balance += valuableList.get(i).getValue();
    	}
    	return balance;
    }

    
    /**
     * Return the capacity of the valuables purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
        return count() == getCapacity();
    }

    /** 
     * Insert a valuable into the purse.
     * The valuable is only inserted if the purse has space for it
     * and the valuable has positive value.  No worthless valuables!
     * @param valuable is a Valuable object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Valuable valuable ) {
        // if the purse is already full then can't insert anything.
    	if(isFull()){ return false; }
    	
    	valuableList.add(valuable);
    	super.setChanged();
    	super.notifyObservers(this);
    	
        return true;
    }
    
    /**
     * Set the strategy from current to the another one
     * @param new strategy
     */
    public void setWithdrawStrategy(WithdrawStrategy strategy){
    	this.strategy = strategy;
    	super.setChanged();
    	super.notifyObservers(this);
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Valuables withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount ) {
        if ( amount < 0 ) return null;
        
        //Withdraw List
        List<Valuable> withList = new ArrayList<Valuable>(); 
        
        //sort valuableList from highest to lowest
        Collections.sort(valuableList, comparator);
        Collections.reverse(valuableList);
        
		/*
        List<Valuable> temp = new ArrayList<Valuable>(); 
        for(int i = 0; i < count(); i++){
        	temp.add(valuableList.get(i));
        }
        
        
        
        /*
        
        //Get the Most Value Coin
        for(int valuableToAdd = 0; valuableToAdd < count(); valuableToAdd++){
        	
        	//check amount
        	if(amount >= valuableList.get(valuableToAdd).getValue()){
        		//add to withdraw list
        		withList.add(valuableList.get(valuableToAdd));
        		//remove from parameter amount value
        		amount -= valuableList.get(valuableToAdd).getValue();
        		//remove from temporary list
            	temp.remove(valuableList.get(valuableToAdd));
        	}
        	
        }
        
        

		// Did we get the full amount?
		if ( amount > 0 )
		{	// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			return null;
		}

		// Success.
		// Since this method returns an array of Coin,
		// create an array of the correct size and copy
		// the Coins to withdraw into the array.
		for(int i = 0; i < withList.size(); i++){
			valuableList.remove(withList.get(i));
		}
		*/
		
		withList = strategy.withdraw(amount, valuableList);
		
		Valuable[] withListArr = new Valuable[withList.size()];
		for(int i = 0; i < withList.size(); i++){
			withListArr[i] = withList.get(i);
		}
		super.setChanged();
		super.notifyObservers(this);
        return withListArr;
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
    	String content = "Purse : { ";
    	for(int i = 0; i < count(); i++){
    		content += valuableList.get(i).getValue() + ", ";
    	}
    	content += "}";
    	return content;
    }

}
