package coinpurse;
 
/**
 * A bank note with a monetary value.
 * You can't change the value of a bank note.
 * @author  Parinvut Rochanavedya 
 * @version 2015.02.10
 */
public class BankNote extends AbstractValuable{

    /** Value of the bank note */
    private static long nextSerialNumber = 1000000;
    private long serialNumber;
    
    /** 
     * Constructor for a new bank note. 
     * @param value is the value for the bank note
     */
    public BankNote( double value ) {
        this.value = value;
        serialNumber = getNextSerialNumber();
    }
    
    /**
     * Return and update the serial number of bank notes
     * @return the next serial number from current
     */
    public static long getNextSerialNumber(){
    	return nextSerialNumber++;
    }
    
    /**
     * Return and the serial number of bank note
     * @return the serial number
     */
    public long getSerialNumber(){
    	return serialNumber;
    }

    /**
     * Print out the bank note and it's value
     */
    public String toString(){
    	return value + " bank note";
    }

    
    
}
